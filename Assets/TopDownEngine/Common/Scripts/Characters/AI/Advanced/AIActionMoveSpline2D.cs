using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace MoreMountains.TopDownEngine
{
    public class AIActionMoveSpline2D : AIAction
    {
        public DOTweenPath path;

        protected TopDownController _controller;
        protected Character _character;
        protected CharacterOrientation2D _orientation2D;
        protected CharacterMovement _characterMovement;

        Vector3 lastPos;

        protected override void Awake()
        {
            base.Awake();
            _controller = this.gameObject.GetComponentInParent<TopDownController>();
        }

        // Start is called before the first frame update
        void Start()
        {
            path.updateType = UpdateType.Manual;
            lastPos = transform.position;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public override void OnEnterState()
        {
            base.OnEnterState();
            path.tween.Play();
        }

        public override void PerformAction()
        {
            if (path)
            {
                path.tween.ManualUpdate(Time.deltaTime, Time.unscaledDeltaTime);
                //Debug.DrawRay(transform.position, (transform.position - lastPos).normalized * 10);
                _controller.SetMovement((transform.position - lastPos).normalized);
                lastPos = transform.position;
            }
        }
    }
}
